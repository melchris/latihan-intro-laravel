<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function Submit(Request $request){
        $firstName = $request['fname'];
        $lastName = $request['lname'];
        return view('halaman.home', compact('firstName','lastName'));
    }
}
